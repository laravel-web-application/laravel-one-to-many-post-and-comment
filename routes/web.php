<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('comments')->group(function () {
    Route::get('/', 'CommentsController@commentPost');
    Route::get('/add/{post_id}', 'CommentsController@showCommentForm');
    Route::post('/', 'CommentsController@addNewCommentPost');
});

Route::prefix('posts')->group(function () {
    Route::get('/', 'PostsController@index');
    Route::get('/add', 'PostsController@addNewPostForm');
    Route::post('/', 'PostsController@addNewPost');
    Route::get('/{post_id}/delete', 'PostsController@delete');
});


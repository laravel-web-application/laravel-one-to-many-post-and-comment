<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row">

        <div class="col-md-8 col-md-offset-2">

            <h1>Create new Comment post</h1>

            <form action="/comments" method="POST">
                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}"/>
                <div class="form-group">
                    <label for="title">Post</label>
                    <input type="hidden" class="form-control" name="id" value="{{$post->id}}" readonly/>
                    <input type="text" class="form-control" name="post_name" value="{{$post->post_name}}" readonly/>
                </div>

                <div class="form-group">
                    <label for="title">Comment <span class="require">*</span></label>
                    <input type="text" class="form-control" name="comment"/>
                </div>

                <div class="form-group">
                    <p><span class="require">*</span> - required fields</p>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        Create
                    </button>
                    <button class="btn btn-default">
                        Cancel
                    </button>
                </div>

            </form>
        </div>

    </div>
</div>

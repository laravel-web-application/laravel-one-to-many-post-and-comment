<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<div class="container">

    <h2>List Posts </h2>
    <a href="/posts/add" class="btn btn-info" role="button">Add New Post</a>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Post Name</th>
            <th scope="col">Post Comment</th>
            <th>ACTION</th>
        </tr>
        </thead>
        <tbody>
        @foreach($post as $p)
            <tr>
                <th scope="row">{{$p->id}}</th>
                <td>{{$p->post_name}}</td>
                <td>
                    @foreach($p->comments as $c)
                        {{$c->comment}},
                    @endforeach
                </td>
                <td>
                    <a href="/comments/add/{{$p->id}}" class="btn btn-info" role="button">Add</a>
                    <a href="/posts/{{$p->id}}/delete" class="btn btn-danger btn-sm"
                       onclick="return confirm('Yakin mau dihapus ?') ">Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

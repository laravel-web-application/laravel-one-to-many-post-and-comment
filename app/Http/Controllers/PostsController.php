<?php

namespace App\Http\Controllers;

use App\Post;
use App\Siswa;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function addNewPostForm()
    {
        return view('addPost');
    }

    public function addNewPost(Request $request)
    {
        $post = new Post();
        $post->post_name = $request->input('post_name');
        $post->save();
        $post = Post::all();
        return view('listPosts', ['post' => $post]);
    }

    public function index()
    {
        $post = Post::all();
        return view('listPosts', ['post' => $post]);
    }

    public function delete($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect('/posts')->with('sukses', 'Data Berhasil dihapus');
    }
}

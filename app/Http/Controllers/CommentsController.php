<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function commentPost()
    {

        $postFirst = Post::find(1);
        $commentsFirst = $postFirst->comments;

        $postSecond = Post::find(2);
        $commentsSecond = $postSecond->comments;

        $allposts = Post::all();
        $allcomments = Comment::all();

        return view('index', compact('commentsFirst', 'commentsSecond', 'allposts', 'allcomments'));

    }

    public function showCommentForm($id)
    {
        $post = Post::find($id);
        $comment = new Comment();

        return view('addComment', ['comment' => $comment, 'post' => $post]);
    }

    public function addNewCommentPost(Request $request)
    {
        $comment = new Comment();
        $comment->comment = $request->input('comment');
        $id = $request->input('id');
        $post = Post::find($id);
        $post->comments()->save($comment);
        $post = Post::all();
        return view('listPosts', ['post' => $post]);
    }

    public function testPost()
    {
        $post = Post::find(3);

        $comment1 = new Comment;
        $comment1->comment = "Hi Naruto, Comment 1";

        $comment2 = new Comment;
        $comment2->comment = "Hi Sasuke, Comment 2";

        $post = $post->comments()->saveMany([$comment1, $comment2]);
    }
}

# Laravel One to Many Post & Comment
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-one-to-many-post-and-comment.git`
2. Go inside the folder: `cd laravel-one-to-many-post-and-comment`
3. Run `cp .env.example .env` and set your desired database name.
4. Run `php artisan migrate`
5. Run `php artisan serve`
6. Open your favorite browser: http://localhost:8000/comment

### Screen shot

Relationship Table

![Relationship Table](img/relationship.png "Relationship Table")

List Posts Page

![List Posts Page](img/posts.png "List Posts Page")

Comments Page

![Comments Page](img/comments.png "Comments Page")

Add New Post Page

![Add New Post](img/add-post.png "Add New Post")

Add Comment Page

![Add Comment Page](img/add-comment.png "Add Comment Page")

